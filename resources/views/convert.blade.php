<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
    .page_break {
        page-break-before: always;
    }

</style>
<div style="text-align: center;">
    @php
        $i = 0;
        $len = count($images);
    @endphp
    @foreach ($images as $image)
        <img src="{{ public_path('storage/' . $image) }}" style="width: 100%" />
        @php
            if (++$i === $len) {
                break;
            }
        @endphp
        <div class="page_break"></div>
    @endforeach
</div>
