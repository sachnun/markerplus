<style>
    .blink {
        animation: blinker 0.7s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
<div class="p-4">
    <img class="d-block mx-auto mb-4" src="{{ asset('storage/img/compress_logo.png') }}" alt="" width="72" height="72">
    <h1 class="fs-2 fw-bold text-center text-primary">
        PDF Co<span class="blink">|</span><span class="text-secondary opacity-25">mpress</span>
    </h1>
    <h5 class="text-center text-primary py-3">
        Kompresi PDF hingga 70% dengan tetap menjaga kualitasnya.
    </h5>
</div>
