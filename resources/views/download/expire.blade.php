@extends('layouts.template')

@section('title', 'Marker Plus - Empty')
@section('content')
    <div class="my-5">
        <div class="p-4">
            <img class="d-block mx-auto mb-4" src="{{ asset('storage/img/crash_logo.png') }}" width="72" height="72">
        </div>

        <div class="my-4 py-3 text-center">
            <span class="text-primary fs-4">File tidak ada atau sudah kadaluarsa.</span>
            <div class="d-flex justify-content-center mt-3">
                <a href="{{ route('beranda') }}" class="d-block btn btn-primary text-white btn px-4">
                    Kembali
                </a>
            </div>
        </div>

    </div>
@endsection
