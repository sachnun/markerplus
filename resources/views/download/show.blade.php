@php
use CodeInc\HumanReadableFileSize\HumanReadableFileSize;
use Illuminate\Support\Facades\URL;
@endphp
@extends('layouts.template')

@section('title', "Marker Plus - $file->original_filename")
@section('content')
    <div class="my-5 pb-5 pb-md-0">
        @include('download.banner')

        <div class="my-4">
            <div class="py-3">
                <p class="text-center text-primary fw-bold">
                    <span class="badge bg-primary">
                        {{ HumanReadableFileSize::getHumanSize($original_size > $size ? $original_size : $size) }}
                    </span>
                    {{ $file->original_filename }}
                </p>
                @if ($save_size)
                    <p class="text-center">
                        Hemat <span class="text-success">{{ HumanReadableFileSize::getHumanSize($save_size) }}</span>
                    </p>
                @endif
            </div>

            <div class="col-10 col-md-4 col-lg-2 mx-auto">
                <small class="text-primary">
                    <i class="bi bi-file-earmark-break-fill"></i>
                    <span id="second">{{ $expired['now'] }}</span> sec sebelum dihapus
                </small>
                <div class="progress my-2" style="height: 5px;">
                    <div id="progress" class="progress-bar" role="progressbar"
                        style="width: {{ ($expired['now'] / $expired['max']) * 100 }}%"
                        aria-valuenow="{{ $expired['now'] }}" aria-valuemin="0" aria-valuemax="{{ $expired['max'] }}">
                    </div>
                </div>
                <a href="{{ URL::signedRoute('generate_download', $file->id) }}"
                    class="d-block btn btn-primary text-white btn-lg px-4 shadow"
                    title="{{ HumanReadableFileSize::getHumanSize($size) }}">
                    Download File
                </a>
            </div>
        </div>
    </div>
    <script>
        onload = function() {
            // progress bar coldown timer
            var timer = setInterval(function() {
                var progress = document.getElementById('progress');
                var percent = progress.getAttribute('aria-valuenow');
                var max = progress.getAttribute('aria-valuemax');
                percent = parseInt(percent) - 1;
                progress.setAttribute('aria-valuenow', percent);
                progress.style.width = (percent / max) * 100 + '%';
                document.getElementById('second').innerHTML = percent;
                if (percent <= 0) {
                    clearInterval(timer);
                    // reload
                    window.location.reload();
                }
            }, 1000);
        }
    </script>
@endsection
