<style>
    .blink {
        animation: blinker 0.7s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
<div class="p-4">
    <img class="d-block mx-auto mb-4" src="{{ asset('storage/img/merge_logo.png') }}" alt="" width="72" height="72">
    <h1 class="fs-2 fw-bold text-center text-primary">
        PDF Me<span class="blink">|</span><span class="text-secondary opacity-25">rge</span>
    </h1>
    <h5 class="text-center text-primary py-3">
        Menggabungkan semua file PDF menjadi satu.
    </h5>
</div>
