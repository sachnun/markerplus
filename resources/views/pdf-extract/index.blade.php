@extends('layouts.template')

@section('title', 'Marker Plus - PDF Extract')
@section('content')
    <div class="mt-5 mb-3">
        @include('pdf-extract.banner')

        <div class="col-12 col-md-8 col-lg-6 d-block mx-auto my-3">
            <div class="progress my-2" style="height: 5px;">
                <div id="progress" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                    style="width: 0%">
                </div>
            </div>
            <form action="{{ route('pdf-extract.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card shadow border-0 p-3">
                    <div class="card-body">
                        <div class="row mb-4 g-3">
                            <div class="col-12">
                                <label class="form-label">Pilih dokumen</label>
                                <input class="form-control @error('files') is-invalid @enderror" type="file" name="files"
                                    accept="application/pdf" multiple required>
                                @error('files')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" checked onclick="return false;">
                            <label class="form-check-label">
                                Setuju dengan <a href="{{ route('privasi') }}" target="_blank"
                                    class="text-decoration-none">ketentuan dan privasi</a>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="my-5 text-center">
                    <button type="submit" class="btn btn-primary text-white col-10 col-md-5">
                        <i class="bi bi-magic"></i> PROSES
                    </button>
                </div>
            </form>
        </div>
    </div>
    @include('layouts.submit')
@endsection
