<style>
    .blink {
        animation: blinker 0.7s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
<div class="p-4">
    <img class="d-block mx-auto mb-4" src="{{ asset('storage/img/extract_logo2.png') }}" alt="" width="72" height="72">
    <h1 class="fs-2 fw-bold text-center text-primary">
        PDF Ext<span class="blink">|</span><span class="text-secondary opacity-25">ract</span>
    </h1>
    <h5 class="text-center text-primary py-3">
        Ekstrak bagian-bagian PDF menjadi gambar.
    </h5>
</div>
