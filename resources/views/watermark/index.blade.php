@extends('layouts.template')

@section('title', 'Marker Plus - Watermark')
@section('content')
    <div class="mt-5 mb-3">
        @include('watermark.banner')

        <div class="col-12 col-md-8 col-lg-6 d-block mx-auto my-3">
            <div class="progress my-2" style="height: 5px;">
                <div id="progress" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                    style="width: 0%">
                </div>
            </div>
            <form action="{{ route('watermark.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card shadow border-0 p-3">
                    <div class="card-body">
                        <div class="mb-3">
                            <label class="form-label">Untuk tujuan apa?</label>
                            <input type="text" id="tujuan" name="tujuan"
                                class="form-control @error('tujuan') is-invalid @enderror" value="{{ old('tujuan') }}">
                            @error('tujuan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label class="form-label">Pilih dokumen atau gambar</label>
                            <input class="form-control @error('files.*') is-invalid @enderror" type="file" name="files[]"
                                accept="image/jpeg, image/png, image/jpg, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, capture=camera"
                                multiple required>
                            <div class="form-text">bisa lebih dari satu namun hasil akan digabung.</div>
                            @error('files.*')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="extract">
                            <label class="form-check-label">
                                Ekstrak hasilnya kedalam bentuk gambar
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" checked onclick="return false;">
                            <label class="form-check-label">
                                Setuju dengan <a href="{{ route('privasi') }}" target="_blank"
                                    class="text-decoration-none">ketentuan dan privasi</a>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="my-5 text-center">
                    <button type="submit" class="btn btn-primary text-white col-10 col-md-5">
                        <i class="bi bi-magic"></i> PROSES
                    </button>
                </div>
            </form>
        </div>
    </div>

    <script>
        // Placeholder Type Witter by lankovova

        function addToPlaceholder(toAdd, el) {
            el.attr('placeholder', el.attr('placeholder') + toAdd);
            return new Promise(resolve => setTimeout(resolve, 100));
        }

        function clearPlaceholder(el) {
            el.attr("placeholder", "");
        }

        function printPhrase(phrase, el) {
            return new Promise(resolve => {
                clearPlaceholder(el);
                let letters = phrase.split('');
                letters.reduce(
                    (promise, letter, index) => promise.then(_ => {
                        if (index === letters.length - 1) {
                            setTimeout(resolve, 1000);
                        }
                        return addToPlaceholder(letter, el);
                    }),
                    Promise.resolve()
                );
            });
        }


        function printPhrases(phrases, el) {
            phrases.reduce(
                (promise, phrase) => promise.then(_ => printPhrase(phrase, el)),
                Promise.resolve()
            );
        }

        // Start typing
        function run() {
            let phrases = [
                "Verifikasi akun e-commerce",
                "Verifikasi pendaftaran Paypal",
                "Lamaran pada PT. Marker Plus",
                "Tentukan tujuanmu apapun itu..",
            ];

            printPhrases(phrases, $('#tujuan'));
        }

        run();
    </script>
    @include('layouts.submit')
@endsection
