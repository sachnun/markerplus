<style>
    .blink {
        animation: blinker 0.7s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
<div class="p-4">
    <img class="d-block mx-auto mb-4" src="{{ asset('storage/img/docu_logo.gif') }}" alt="" width="92" height="72">
    <h1 class="fs-2 fw-bold text-center text-primary">
        Waterm<span class="blink">|</span><span class="text-secondary opacity-25">ark <sup>+</sup></span>
    </h1>
</div>
