@extends('layouts.template')

@section('content')
    <style>
        .hover-white:hover {
            color: white;
        }

    </style>
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5 px-3">
        <div class="col-12 col-sm-8 col-lg-5 d-flex justify-content-center">
            <img src="{{ asset('storage/img/doc_logo.png') }}" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes"
                width="300" height="200">
        </div>
        <div class="col-lg-6">
            <h1 class="fs-1 fw-bold lh-1 mb-3 text-primary">Yakin, dokumenmu aman?</h1>
            <p class="lead">ayo tambahkan watermark untuk memberi perlindungan</p>
            <div class="d-grid gap-2 d-md-flex justify-content-md-start pt-4">
                <a href="{{ route('watermark.index') }}" class="btn btn-primary text-white btn-lg px-4 me-md-2">
                    <i class="bi bi-file-earmark-lock2-fill"></i> Beri Watermark
                </a>
                <a href="{{ route('fitur') }}" class="btn btn-outline-primary btn-lg px-4 hover-white">
                    Fitur
                </a>
            </div>
        </div>
    </div>
@endsection
