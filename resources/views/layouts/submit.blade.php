<script>
    // on button submit change with progress
    $('form').submit(function() {
        $('#progress').css('width', '95%');
        // disable all input and button
        $('input').prop('readonly', true);
        $('select').prop('disabled', true);
        $('button').prop('disabled', true);
        // input file pointer-events: none;
        $('input[type=file]').css('pointer-events', 'none');
        // input checkbox onlick return false
        $('input[type=checkbox]').click(function() {
            return false;
        });
    });
</script>
