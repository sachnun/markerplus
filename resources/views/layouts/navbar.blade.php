<li class="nav-item d-none d-md-inline">
    <a href="{{ route('beranda') }}" class="nav-link @if (Request::is('/')) active @endif">Beranda</a>
</li>
{{-- mobile --}}
<li class="nav-item d-md-none">
    <a href="{{ route('beranda') }}" class="nav-link @if (Request::is('/')) active @endif">
        <i class="bi bi-house-fill"></i>
    </a>
</li>

<li class="nav-item d-none d-md-inline">
    <a href="{{ route('watermark.index') }}" class="nav-link @if (Request::is('watermark*')) active @endif">Watermark</a>
</li>
{{-- mobile --}}
<li class="nav-item d-md-none">
    <a href="{{ route('watermark.index') }}" class="nav-link @if (Request::is('watermark*')) active @endif">
        <i class="bi bi-textarea-t"></i>
    </a>
</li>

<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle @if (Request::is('pdf*')) active @endif" href="#" id="navbarDropdown" role="button"
        data-bs-toggle="dropdown" aria-expanded="false">
        PDF
    </a>
    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li>
            <a class="dropdown-item" href="{{ route('pdf-compress.index') }}">
                PDF Compress
            </a>
        </li>
        <li>
            <a class="dropdown-item" href="{{ route('pdf-extract.index') }}">
                PDF Extract
            </a>
        </li>
        <li>
            <a class="dropdown-item" href="{{ route('pdf-merge.index') }}">
                PDF Merge
            </a>
        </li>
        <li>
            <a class="dropdown-item disabled" href="#" data-bs-toggle="modal" data-bs-target="#coming-soon-modal">
                PDF Recognition
            </a>
        </li>
        <li>
            <hr class="dropdown-divider">
        </li>
        <li>
            <a class="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#coming-soon-modal">
                ANY <i class="bi bi-arrow-right"></i> PDF
            </a>
        </li>
    </ul>
</li>
<li class="nav-item d-none d-md-inline">
    <a href="{{ route('fitur') }}" class="nav-link @if (Request::is('fitur*')) active @endif">Fitur</a>
</li>
<li class=" nav-item d-none d-md-inline">
    <a href="{{ route('privasi') }}" class="nav-link @if (Request::is('privasi*')) active @endif">Privasi</a>
</li>

<li class="nav-item d-none d-md-inline">
    <button class="nav-link" data-bs-toggle="modal" data-bs-target="#tentang-modal">Tentang</button>
</li>
{{-- mobile --}}
<li class="nav-item d-md-none">
    <button class="nav-link" data-bs-toggle="modal" data-bs-target="#tentang-modal">
        <i class="bi bi-info-circle-fill"></i>
    </button>
</li>
