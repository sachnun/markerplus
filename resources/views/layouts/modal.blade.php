    {{-- Modal Tentang --}}
    <div class="modal fade" id="tentang-modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content rounded-6 shadow p-2">
                <div class="modal-header border-bottom-0">
                    <h5 class="modal-title">Tentang Kami</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body py-0">
                    <p>Kami sangat prihatin atas maraknya kebocoran dan penyalahgunaan data.</p>
                    <p>Tujuan kami membuat layanan ini yaitu untuk mempermudah pengguna dalam memberikan
                        perlindungan
                        lebih pada dokumen penting sebelum melakukan verifikasi atau apapun itu.</p>
                    <p>Dirancang dan dibuat oleh</p>
                    <ul>
                        <li>
                            <a href="https://fb.com/sachnun" class="text-decoration-none" target="_blank">
                                Sachnun Kusmayadi
                            </a>
                        </li>
                        <li>
                            <a href="https://fb.com/heinstarkmz" class="text-decoration-none" target="_blank">
                                Adi Bastian
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Coming Soon --}}
    <div class="modal fade" id="coming-soon-modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content rounded-6 shadow p-2">
                <div class="modal-header border-bottom-0">
                    <h5 class="modal-title">Coming Soon</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body py-0">
                    <p>Sedang dalam proses pengembangan.</p>
                </div>
            </div>
        </div>
