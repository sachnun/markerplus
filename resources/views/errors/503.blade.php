<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Marker Plus</title>
</head>

<body>
    <style>
        @media only screen and (max-width: 767px) {
            .description {
                text-align: center;
            }
        }

    </style>
    <div class="col-10 col-md-5 mx-auto" style="margin-top: 15%">
        <div class="row d-flex align-items-center" style="color: #d1495b">
            <div class="col-6 col-md-3 mx-auto">
                <img src="{{ asset('storage/img/doc_logo.png') }}" alt="image-logo.png" class="img-fluid">
            </div>
            <div class="col-12 col-md-9 mt-4 mt-md-0 description">
                <h1>Marker Plus</h1>
                <p>Layanan kami akan segera kembali...</p>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>
