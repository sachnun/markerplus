<?php

namespace App\Models;

use App\Http\Traits\UuidGenerator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StorageFile extends Model
{
    use HasFactory, SoftDeletes, UuidGenerator;

    // guarded
    protected $guarded = ['id'];

    // expired
    function expired()
    {
        // mendapatkan detik dari tanggal expired_at
        $expired_at = Carbon::parse($this->expired_at);
        $now = Carbon::now();
        if ($expired_at < $now) {
            $diff = false;
        } else {
            $diff = $expired_at->diffInSeconds($now);
        }

        return $diff;
    }
}
