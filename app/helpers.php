<?php

use Illuminate\Support\Facades\Http;

function secretkey()
{
    $response = Http::get("https://convert-secretkey-san.dakunesu.repl.co/getkey");
    if ($response->status() == 200) {
        return json_decode($response->body())->key;
    } else {
        return die('Maaf, sistem tidak bekerja untuk saat ini.');
    }
}
