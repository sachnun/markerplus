<?php

namespace App\Jobs;

use App\Models\StorageFile;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ExpiredStorageFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $storageFile;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(StorageFile $storageFile)
    {
        $this->storageFile = $storageFile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // hapus encrypt_path dari storage
        Storage::disk('local')->delete($this->storageFile->encrypt_path);
        // hapus data dari storage_files
        $this->storageFile->delete();
    }


    public function uniqueId()
    {
        return $this->storageFile->id;
    }
}
