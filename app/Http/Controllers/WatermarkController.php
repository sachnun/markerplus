<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\StorageFile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Jobs\ExpiredStorageFile;
use Barryvdh\DomPDF\Facade as PDF;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use SoareCostin\FileVault\Facades\FileVault;
use Illuminate\Support\Facades\Http;

class WatermarkController extends Controller
{
    public function index()
    {
        return view('watermark.index');
    }

    public function store(Request $request)
    {
        // validate
        $request->validate([
            'tujuan' => 'required|string|max:60',
            'files.*' => 'required|mimes:jpeg,png,jpg,pdf,doc,docx,xls,xlsx,ppt,pptx',
            'extract' => 'nullable'
        ]);

        // check extension file
        $first = true;
        $original_size = 0;
        foreach ($request->file('files') as $file) {
            if ($first == true) {
                // get original filename without extension
                $original_filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $first = false;
            }
            $original_size += $file->getSize();
            $extension = $file->getClientOriginalExtension();

            $response = Http::attach('File', file_get_contents($file), Str::random(5) . '.' . $extension);
            $response = $response->post("https://v2.convertapi.com/convert/" . $extension . "/to/pdf?Secret=" . secretkey());
            // check response status code not 200
            if ($response->status() != 200) {
                return 'Terjadi masalah pada sistem, silahkan coba lagi.';
            }
            $pdf_files[] = base64_decode(json_decode($response->body())->Files[0]->FileData);
        }

        // merge pdf if pdf_file > 1
        if (count($pdf_files) > 1) {
            $response = Http::withoutRedirecting();
            foreach ($pdf_files as $i => $pdf) {
                $response = $response->attach('Files[' . $i . ']', $pdf, Str::random(5) . '.pdf');
            }
            $response = $response->post("https://v2.convertapi.com/convert/pdf/to/merge?Secret=" . secretkey());
            // check response status code not 200
            if ($response->status() != 200) {
                return 'Terjadi masalah pada sistem, silahkan coba lagi.';
            }
            $pdf = base64_decode(json_decode($response->body())->Files[0]->FileData);
        } else {
            $pdf = $pdf_files[0];
        }

        // get today's date dd-mm-yyyy using carbon
        $date = Carbon::now()->format('d-m-Y');

        // watermark pdf
        $response  = Http::attach('File', $pdf, Str::random(5) . '.pdf');
        $response = $response->post("https://v2.convertapi.com/convert/pdf/to/watermark?Secret=" . secretkey(), [
            'Text' => $date . '%N%%N%'  . $request->tujuan, // text watermark
            'TextAlign' => 'center',
            'WordWrap' => 'true',
            'TextRenderingMode' => 'stroketext',
            'FontSize' => '25', // font size
            'Opacity' => '20'
        ]);

        // check response status code not 200
        if ($response->status() != 200) {
            return 'Terjadi masalah pada sistem, silahkan coba lagi.';
        }

        $response = base64_decode(json_decode($response->body())->Files[0]->FileData);

        // compress pdf
        $response = Http::attach('File', $response, Str::random(10) . '.pdf')
            ->post("https://v2.convertapi.com/convert/pdf/to/compress?Secret=" . secretkey(), [
                'ColorImageQuality' => 70,
                'ColorImageDownsample' => 'true',
                'GrayscaleImageQuality' => 70,
                'GrayscaleImageDownsample' => 'true',
            ]);

        // check response status code not 200
        if ($response->status() != 200) {
            return 'Terjadi masalah pada sistem, silahkan coba lagi.';
        }

        $response = base64_decode(json_decode($response->body())->Files[0]->FileData);

        // extract image from pdf
        $extension = 'pdf';
        if ($request->extract == True) {
            $response = $this->extract($response, $original_filename);
            $extension = 'zip';
        }

        // generate encrypted filename
        $encrypt_path = 'files/' . Str::random(50) . ".$extension";

        // save response to local storage
        Storage::disk('local')->put($encrypt_path, $response);
        $size = Storage::disk('local')->size($encrypt_path);

        // encrypt file
        FileVault::encrypt($encrypt_path);

        $expired = Carbon::now()->addMinute(3);

        // save to table storage_files
        $file = StorageFile::create([
            'encrypt_path' => $encrypt_path . '.enc',
            'original_filename' => str_replace(' ', '_', $original_filename) . '_markerplus.' . $extension,
            'original_size' => $original_size,
            'size' => $size,
            'expired_at' => $expired
        ]);

        // dispatch job to expired file
        ExpiredStorageFile::dispatch($file)->delay($expired);

        // redirect to download page
        return redirect()->route('download.show', $file->id);
    }
    private function extract($file, $original_filename)
    {
        $response = Http::attach('File', $file, Str::random(10) . '.pdf')
            ->post("https://v2.convertapi.com/convert/pdf/to/jpg?Secret=" . secretkey());

        // check response status code not 200
        if ($response->status() != 200) {
            return 'Terjadi masalah pada sistem, silahkan coba lagi.';
        }

        $response_body = json_decode($response->body())->Files;

        $response = Http::withoutRedirecting();
        foreach ($response_body as $i => $file) {
            $file = base64_decode($file->FileData);
            $response = $response->attach('Files[' . $i . ']', $file, $original_filename . "_($i).jpg");
        }

        $response = $response->post("https://v2.convertapi.com/convert/any/to/zip?Secret=" . secretkey());
        $response = base64_decode(json_decode($response->body())->Files[0]->FileData);

        return $response;
    }
}
