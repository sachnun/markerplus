<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Models\StorageFile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Jobs\ExpiredStorageFile;
use function GuzzleHttp\json_decode;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use SoareCostin\FileVault\Facades\FileVault;

class PdfExtractController extends Controller
{
    // index
    public function index()
    {
        return view('pdf-extract.index');
    }
    public function store(Request $request)
    {
        // // validate
        // $request->validate([
        //     'files' => 'required|mimes:pdf',
        // ]);

        // variable file form request file
        $file = $request->file('files');

        // get original filename without extension
        $original_filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $original_size = $file->getSize();

        $response = Http::attach('File', file_get_contents($file), Str::random(10) . '.pdf')
            ->post("https://v2.convertapi.com/convert/pdf/to/jpg?Secret=" . secretkey());

        // check response status code not 200
        if ($response->status() != 200) {
            return 'Terjadi masalah pada sistem, silahkan coba lagi.';
        }

        $response_body = json_decode($response->body())->Files;

        $response = Http::withoutRedirecting();
        foreach ($response_body as $i => $file) {
            $file = base64_decode($file->FileData);
            $response = $response->attach('Files[' . $i . ']', $file, $original_filename . "_($i).jpg");
        }

        $response = $response->post("https://v2.convertapi.com/convert/any/to/zip?Secret=" . secretkey());

        $response = base64_decode(json_decode($response->body())->Files[0]->FileData);

        // generate encrypted filename
        $encrypt_path = 'files/' . Str::random(50) . '.pdf';

        // save response to local storage
        Storage::disk('local')->put($encrypt_path, $response);
        $size = Storage::disk('local')->size($encrypt_path);

        // encrypt file
        FileVault::encrypt($encrypt_path);

        $expired = Carbon::now()->addMinute(3);

        // save to table storage_files
        $file = StorageFile::create([
            'encrypt_path' => $encrypt_path . '.enc',
            'original_filename' => str_replace(' ', '_', $original_filename) . '_markerplus.zip',
            'original_size' => $original_size,
            'size' => $size,
            'expired_at' => $expired
        ]);

        // dispatch job to expired file
        ExpiredStorageFile::dispatch($file)->delay($expired);

        // redirect to download page
        return redirect()->route('download.show', $file->id);
    }
}
