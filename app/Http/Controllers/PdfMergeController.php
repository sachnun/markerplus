<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\StorageFile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Jobs\ExpiredStorageFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use SoareCostin\FileVault\Facades\FileVault;

class PdfMergeController extends Controller
{
    public function index()
    {
        return view('pdf-merge.index');
    }
    public function store(Request $request)
    {
        // validate
        $request->validate([
            'files.*' => 'required|mimes:pdf',
        ]);

        // check count file
        if (count($request->file('files')) < 2) {
            return redirect()->back()->withErrors('files', 'Minimum 2 files uploaded.');
        }

        $first = true;
        $original_size = 0;
        $response = Http::withoutRedirecting();
        foreach ($request->file('files') as $i => $file) {
            if ($first == true) {
                // get original filename without extension
                $original_filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $first = false;
            }
            $original_size = $original_size + $file->getSize();
            $response = $response->attach('Files[' . $i . ']', file_get_contents($file), Str::random(5) . '.pdf');
        }

        $response = $response->post("https://v2.convertapi.com/convert/pdf/to/merge?Secret=" . secretkey());

        // check response status code not 200
        if ($response->status() != 200) {
            return 'Terjadi masalah pada sistem, silahkan coba lagi.';
        }

        $response = base64_decode(json_decode($response->body())->Files[0]->FileData);

        // generate encrypted filename
        $encrypt_path = 'files/' . Str::random(50) . '.pdf';

        // save response to local storage
        Storage::disk('local')->put($encrypt_path, $response);
        $size = Storage::disk('local')->size($encrypt_path);

        // encrypt file
        FileVault::encrypt($encrypt_path);

        $expired = Carbon::now()->addMinute(3);

        // save to table storage_files
        $file = StorageFile::create([
            'encrypt_path' => $encrypt_path . '.enc',
            'original_filename' => str_replace(' ', '_', $original_filename) . '_digabung_markerplus.pdf',
            'original_size' => $original_size,
            'size' => $size,
            'expired_at' => $expired
        ]);

        // dispatch job to expired file
        ExpiredStorageFile::dispatch($file)->delay($expired);

        // redirect to download page
        return redirect()->route('download.show', $file->id);
    }
}
