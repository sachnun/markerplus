<?php

namespace App\Http\Controllers;

use App\Models\StorageFile;
use App\Jobs\ExpiredStorageFile;
use SoareCostin\FileVault\Facades\FileVault;

class DownloadController extends Controller
{
    public function show($id)
    {
        // get storage file
        $file = StorageFile::find($id);

        // check not found
        if (!$file) {
            return view('download.expire');
        }

        // check expired
        if (!$file->expired()) {
            ExpiredStorageFile::dispatchNow($file);
            return view('download.expire');
        }

        $save_size = $file->original_size - $file->size;

        // view show
        return view('download.show', [
            'file' => $file,
            'size' => $file->size,
            'original_size' => $file->original_size,
            'save_size' => $save_size > 0 ? $save_size : false,
            'expired' => [
                'now' => $file->expired(),
                'max' => '180' // 3 minutes
            ]
        ]);
    }

    public function files($id)
    {
        if (!request()->hasValidSignature()) {
            abort(401);
        }

        $file = StorageFile::find($id);

        // check if file not exists or expired
        if (!$file || !$file->expired()) {
            return redirect()->route('download.show', $id);
        }

        $base64 = base64_encode(json_encode([
            'encrypt_path' => $file->encrypt_path,
            'original_filename' => $file->original_filename,
        ]));

        return redirect()->to('/files?parser=' . $base64);
    }

    public function stream()
    {
        // check request is empty
        if (!request()->has('parser')) {
            return redirect()->route('beranda');
        }

        // decode json from request
        $parser = json_decode(base64_decode(request('parser')), true);

        return response()->streamDownload(function () use ($parser) {
            FileVault::streamDecrypt($parser['encrypt_path']);
        }, $parser['original_filename'], [
            'Content-Type' => 'application/octet-stream'
        ]);
    }
}
