<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\StorageFile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Jobs\ExpiredStorageFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use SoareCostin\FileVault\Facades\FileVault;

class PdfCompressController extends Controller
{
    private function kompresi($setting)
    {
        switch ($setting) {
            case 'low':
                return [
                    'ColorImageQuality' => 70,
                    'GrayscaleImageQuality' => 70,
                ];
                break;
            case 'medium':
                return [
                    'ColorImageQuality' => 50,
                    'ColorImageDownsample' => 'true',
                    'GrayscaleImageQuality' => 50,
                    'GrayscaleImageDownsample' => 'true',
                ];
                break;
            case 'high':
                return [
                    'ColorImageQuality' => 25,
                    'ColorImageDownsample' => 'true',
                    'GrayscaleImageQuality' => 25,
                    'GrayscaleImageDownsample' => 'true',
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function index()
    {
        return view('pdf-compress.index');
    }
    public function store(Request $request)
    {
        // validate
        $request->validate([
            'kompresi' => 'default:medium|in:low,medium,high',
            'files' => 'required|mimes:pdf',
        ]);

        // variable file form request file
        $file = $request->file('files');

        // get original filename without extension
        $original_filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $original_size = $file->getSize();

        $setting = $this->kompresi($request->kompresi);

        $response = Http::attach('File', file_get_contents($file), Str::random(10) . '.pdf')
            ->post("https://v2.convertapi.com/convert/pdf/to/compress?Secret=" . secretkey(), $setting);

        // check response status code not 200
        if ($response->status() != 200) {
            return 'Terjadi masalah pada sistem, silahkan coba lagi.';
        }

        $response = base64_decode(json_decode($response->body())->Files[0]->FileData);

        // generate encrypted filename
        $encrypt_path = 'files/' . Str::random(50) . '.pdf';

        // save response to local storage
        Storage::disk('local')->put($encrypt_path, $response);
        $size = Storage::disk('local')->size($encrypt_path);

        // encrypt file
        FileVault::encrypt($encrypt_path);

        $expired = Carbon::now()->addMinute(3);

        // save to table storage_files
        $file = StorageFile::create([
            'encrypt_path' => $encrypt_path . '.enc',
            'original_filename' => str_replace(' ', '_', $original_filename) . '_dikompresi_markerplus.pdf',
            'original_size' => $original_size,
            'size' => $size,
            'expired_at' => $expired
        ]);

        // dispatch job to expired file
        ExpiredStorageFile::dispatch($file)->delay($expired);

        // redirect to download page
        return redirect()->route('download.show', $file->id);
    }
}
