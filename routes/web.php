<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DownloadController;
use App\Http\Controllers\PdfMergeController;
use App\Http\Controllers\WatermarkController;
use App\Http\Controllers\PdfCompressController;
use App\Http\Controllers\PdfExtractController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('beranda');
})->name('beranda');

Route::resource('/download', DownloadController::class)
    ->only(['show']);

Route::resource('/watermark', WatermarkController::class)
    ->only(['index', 'store']);

Route::resource('/pdf/compress', PdfCompressController::class)
    ->only(['index', 'store'])
    ->names('pdf-compress');

Route::resource('/pdf/extract', PdfExtractController::class)
    ->only(['index', 'store'])
    ->names('pdf-extract');

Route::resource('/pdf/merge', PdfMergeController::class)
    ->only(['index', 'store'])
    ->names('pdf-merge');

Route::get('/fitur', function () {
    return view('fitur');
})->name('fitur');

Route::get('/privasi', function () {
    return view('privasi');
})->name('privasi');

Route::get('/sample', function () {
    return response()
        ->download((public_path('storage/img/sample_ktp.png')), 'sample_ktp.png');
});

Route::get('/files', [DownloadController::class, 'stream']);

Route::get('/files/{id}', [DownloadController::class, 'files'])
    ->name('generate_download');
